package com.zuitt.batch212;

public class Identifier {

    public static void main(String[] args) {

//  Identifiers act as containers for values that are to be used by the program and manipulated further as needed.

//  Identifier declaration
        int myNum;

//  initialization
//  literals/constant - any constant value which can be assigned to the identifier

        int x = 100;

        myNum = 29;
        System.out.println(myNum);
//  Reassign the literals
        myNum = 1;
        System.out.println(myNum);

        int age;
        char middle_name;

 // Constants - the value / literals will not be changed

        final int PRINCIPAL = 1000;
//        PRINCIPAL = 500; cannot assign a value to a final
        System.out.println(PRINCIPAL);

//  Primitive data types
//  Single quotes for Character Data Type
        char letter = 'A';
        boolean isMarried = false;
        byte students = 127;
        short seats = 32767;
        int localPopulation = 2_146_276_827;
//  underscore (_) will not affect the code
        System.out.println(localPopulation);

        long worldPopulation = 2_146_276_827L;
//  L - long literals

//  Decimal Points
        float price = 12.99F;
        double temperature = 12745.43254;

//  To grab data type of an identifier, we can use getClass(), we need to convert unless it is a string
        System.out.println(((Object)temperature).getClass());


//  Non-Primitive data types / referencing data types
//  String, Arrays, CLasses, Interface
//  String uses double quotations and starts with Capital S
        String name = "John Doe";
        System.out.println(name);

        String editedName = name.toLowerCase();
        System.out.println(editedName);

        System.out.println(name.getClass());

//  Escape characters (\n, \")
//  use double \\ so that you can add that to the string
        System.out.println("c:\\windows\\users");


//  TypeCasting - the process of converting of data types
//  Implicit Casting / Widening Type Casting - automatic conversion by the compiler without programmers interference - triggered if the conversion involves a smaller data type to the larger type
//  byte -> short -> int -> long -> float -> double

//  convert byte (smaller size) to double (larger size)
        byte num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println(total);



//  Explicit Casting / Narrowing Type Casting - we manually convert one data type into another using the parenthesis
//  If larger size to smaller size data type
//  Converting double into an int
        int num3 = 6;
        double num4 = 7.4;
        int anotherTotal = (int) (num3 + num4);
        System.out.println(anotherTotal);

//  Concatenation - both strings will be concatenated

        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println(mathGrade + englishGrade);

//  Converting strings into integers

        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(totalGrade);


//  Converting integers to string
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade.getClass());




    }

}
