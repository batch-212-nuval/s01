package com.zuitt.batch212;

import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {

//  Scanner Class - to get user input and it is imported from the java.util package

        Scanner appScanner = new Scanner(System.in);
        System.out.println("What's your name?");
        String myName = appScanner.nextLine().trim();
//  trim() is used for removing white space before and after the words
        System.out.println("User name is: " +myName);

        System.out.println("What's your age?");
        int myAge = appScanner.nextInt();
        System.out.println("Age of the user: " + myAge);

        double myAge2 = appScanner.nextDouble();
        System.out.println("Age of the user: " + myAge2);


    }


}
